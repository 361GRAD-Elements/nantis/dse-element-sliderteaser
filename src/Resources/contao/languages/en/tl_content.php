<?php

/**
 * 361GRAD Element Sliderteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elements';
$GLOBALS['TL_LANG']['CTE']['dse_sliderteaser'] = ['Slider Teaser', 'Slider Teaser.'];

$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];
$GLOBALS['TL_LANG']['tl_content']['textcolor_legend']   = 'Text color settings';
$GLOBALS['TL_LANG']['tl_content']['dse_textcolor']  = ['Text color', 'Please choose the color of the text'];
$GLOBALS['TL_LANG']['tl_content']['dse_textcolor-black']  = 'Black';
$GLOBALS['TL_LANG']['tl_content']['dse_textcolor-white']  = 'White';
$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle']  = ['CTA Button Title', 'The link title will be displayed on hover.'];
$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref']   = ['CTA Button Link', 'Please enter a web address (http://…), an e-mail address (mailto:…) or an insert'];
$GLOBALS['TL_LANG']['tl_content']['dse_bgImage']   = ['Background Image', 'Please select a file or folder from the files directory.'];
$GLOBALS['TL_LANG']['tl_content']['sliderteaser_legend']   = 'Teaser settings';