<?php

/**
 * 361GRAD Element Sliderteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_sliderteaser'] =
    'Dse\\ElementsBundle\\ElementSliderteaser\\Element\\ContentDseSliderteaser';

// Cascading Style Sheets
$GLOBALS['TL_CSS']['dse_sliderteaser'] = 'bundles/dseelementsliderteaser/css/fe_ce_dse_sliderteaser.css';

