<?php

/**
 * 361GRAD Element Sliderteaser
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_sliderteaser'] =
    '{type_legend},type,headline,dse_subheadline;' .
    '{text_legend},dse_text;' .
    '{textcolor_legend},dse_textcolor;' .
    '{image_legend},addImage;' .
    '{sliderteaser_legend},dse_ctaHref,dse_ctaTitle;' .
    '{invisible_legend:hide},invisible,start,stop';

// Element subpalettes
$GLOBALS['TL_DCA']['tl_content']['subpalettes']['addImage'] = 'singleSRC,alt,title,size,imagemargin,fullsize,floating';

// ELement fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_text']        = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_text'],
    'inputType' => 'textarea',
    'eval'      => [
        'mandatory' => false,
        'tl_class'  => 'clr',
        'rte'       => 'tinyMCE',
    ],
    'sql'       => 'text NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaHref'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaHref'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50 wizard'
    ],
    'wizard'    => [
        [
            'tl_content',
            'pagePicker',
        ]
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_ctaTitle'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_ctaTitle'],
    'inputType' => 'text',
    'eval'      => [
        'mandatory' => false,
        'tl_class' => 'w50',
    ],
    'sql'       => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_textcolor'] = array
    (
    'label' => &$GLOBALS['TL_LANG']['tl_content']['dse_textcolor'],
    'exclude' => true,
    'inputType' => 'radio',
    'options' => array(
        'color-black' => &$GLOBALS['TL_LANG']['tl_content']['dse_textcolor-black'],
        'color-white' => &$GLOBALS['TL_LANG']['tl_content']['dse_textcolor-white']),
    'eval' => array('tl_class' => ''),
    'sql' => "varchar(90) NOT NULL default 'color-black'"
);